﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace hw4
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            PopulateListView();
        }

        private void MyPokeList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ListView tappedPokeListView = (ListView)sender;
            Pokemon tappedPokemon = (Pokemon)tappedPokeListView.SelectedItem;
            Uri tappedPokeUri = new Uri(tappedPokemon.PokeUrl);
            Device.OpenUri(tappedPokeUri);
        }

        private void PopulateListView()
        {
            ObservableCollection<Pokemon> myPokeCollection = new ObservableCollection<Pokemon>()
            {
                new Pokemon()
                {
                    PokeName = "Bulbasaur",
                    PokeDexEntry = "#001, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("bulbasaur.png"),   //can't name images by single number eg "1.png", causes problems with android
                    PokeUrl = "https://pokemondb.net/pokedex/bulbasaur",

                },
                new Pokemon()
                {
                    PokeName = "Ivysaur",
                    PokeDexEntry = "#002, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("ivysaur.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/ivysaur",

                },
                new Pokemon()
                {
                    PokeName = "Venusaur",
                    PokeDexEntry = "#003, the 'Seed Pokémon'",
                    PokeImage =ImageSource.FromFile("venusaur.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/venusaur",

                },
                new Pokemon()
                {
                    PokeName = "Charmander",
                    PokeDexEntry = "#004, the 'Lizard Pokémon'",
                    PokeImage =ImageSource.FromFile("charmander.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charmander",

                },
                new Pokemon()
                {
                    PokeName = "Charmeleon",
                    PokeDexEntry = "#005, the 'Flame Pokémon'",
                    PokeImage =ImageSource.FromFile("charmeleon.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charmeleon",

                },
                new Pokemon()
                {
                    PokeName = "Charizard",
                    PokeDexEntry = "#006, the 'Flame Pokémon'",
                    PokeImage =ImageSource.FromFile("charizard.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/charizard",

                },
                new Pokemon()
                {
                    PokeName = "Squirtle",
                    PokeDexEntry = "#007, the 'Tiny Turtle Pokémon'",
                    PokeImage =ImageSource.FromFile("squirtle.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/squirtle",

                },
                new Pokemon()
                {
                    PokeName = "Wartortle",
                    PokeDexEntry = "#008, the 'Turtle Pokémon'",
                    PokeImage =ImageSource.FromFile("wartortle.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/wartortle",

                },
                new Pokemon()
                {
                    PokeName = "Blastoise",
                    PokeDexEntry = "#009, the 'Shellfish Pokémon'",
                    PokeImage =ImageSource.FromFile("blastoise.png"),
                    PokeUrl = "https://pokemondb.net/pokedex/blastoise",

                },
            };

            myPokeListView.ItemsSource = myPokeCollection;
        }
    }
}
