﻿using Xamarin.Forms;

namespace hw4
{
    class Pokemon
    {
        public string PokeName
        {
            get;
            set;
        }
        public string PokeDexEntry
        {
            get;
            set;
        }
        public ImageSource PokeImage
        {
            get;
            set;
        }
        public string PokeUrl
        {
            get;
            set;
        }
    }
}
